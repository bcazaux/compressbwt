#include <sdsl/lcp.hpp>
#include <sdsl/suffix_arrays.hpp>
#include <sdsl/suffix_trees.hpp>
#include <iostream>
#include <typeinfo>
// #include <typeinfo>
// #include <iostream>

using namespace std;
using namespace sdsl;

typedef cst_sct3<> cst_t;
















int main(int argc, char* argv[])
{


    if (argc < 2) {
        cout << "Usage: " << argv[0] << " file" << endl;
        return 1;
    }
    string file = argv[1];

    std::ifstream input( file );

    string mot = "$";



    for( std::string line; getline( input, line ); )
    {
        mot += line + "$";
    }


    // cout << mot << endl;

    cache_config cc(false); // do not delete temp files after csa construction
    csa_wt<> cst;
    construct_im(cst, mot, 1);

    cout << "BWT create" << endl;

    cc.delete_files = true; // delete temp files after lcp construction
    lcp_wt<> lcp;
    construct_im(lcp, mot, 1);


    int lf [cst.bwt.size()];


    for(int i=0;i<cst.size();i++){
        if (cst.lf[cst.lf[i]] == 0){
            lf[i] = 0;
        }
        else{
            lf[i] = cst.lf[i];
        }
    }

    int rlf [cst.bwt.size()];

    for(int i=0;i<cst.size();i++){
        if (false){
            rlf[i] = 0;
        }
        else{
            rlf[lf[i]] = i;
        }
    }



    int lrs [cst.bwt.size()];
    int nb;
    int k;
    int nb_max;
    bool aa;



    for(int i=0;i<cst.size();i++){
        if (cst[i] != 0 && cst.bwt[i] == '$'){
            nb = 0;
            k = i;
            aa = true;
            while (aa){
                k = rlf[k];
                nb += 1;
                if (cst.bwt[k] == '$'){
                    aa = false;
                }
            }
            nb_max = nb-1;
            nb = 0;
            k = i;
            aa = true;
            while (aa){
                lrs[k] = nb_max - nb;
                k = rlf[k];
                nb += 1;
                if (cst.bwt[k] == '$'){
                    aa = false;
                }
            }
        }
    }

    cout << "LRS create" << endl;


    int lcpp [cst.bwt.size()];

    for(int i=0;i<cst.size();i++){
        if (lcp[i] < lrs[i]){
            lcpp[i] = lcp[i];
        }
        else{
            lcpp[i] = lrs[i];
        }
    }

    cout << "LCP create" << endl;





    // Ecriture dans fichier
    std::fstream fbwt;
    fbwt.open (file+".bwt", std::fstream::in | std::fstream::out | std::fstream::trunc  );

    for(int i=1;i<cst.size();i++){
        // if (true){
        if (cst[i] != 0){
            fbwt << cst.bwt[i];
        }
        if (i<cst.size()-1 && cst[i] != 0 ){
            fbwt << "\t";
        }
    }
    fbwt << "\n";


    fbwt.close();

	cout << "Creation file " << file+".bwt" << endl;

    std::fstream flcp;
    flcp.open (file+".lcp", std::fstream::in | std::fstream::out | std::fstream::trunc );

    for(int i=1;i<cst.size();i++){
        // if (true){
        if (cst[i] != 0){
            flcp << lcpp[i];
        }
        if (i<cst.size()-1 && cst[i] != 0 ){
            flcp << "\t";
        }
    }
    flcp << "\n";

    flcp.close();

	cout << "Creation file " << file+".lcp" << endl;

    std::fstream flrs;
    flrs.open (file+".lrs", std::fstream::in | std::fstream::out | std::fstream::trunc  );

    for(int i=1;i<cst.size();i++){
        // if (true){
        if (cst[i] != 0){
            flrs << lrs[i];
        }
        if (i<cst.size()-1 && cst[i] != 0 ){
            flrs << "\t";
        }
    }
    flrs << "\n";

    flrs.close();

	cout << "Creation file " << file+".lrs" << endl;
}
