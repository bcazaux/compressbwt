# using SuffixArrays


function BWT_file(file_bwt,file_lcp,file_lrs)
    fb = open(file_bwt)
    sb2 = readstring(fb)
    sb = sb2[1:length(sb2)-1]
    close(fb)
    bwt_mot = [x[1] for x in split(sb,"\t")]
    fl = open(file_lcp)
    sl = readstring(fl)
    close(fl)
    lcp_mot = [parse(Int,x) for x in split(sl,"\t")]
    fr = open(file_lrs)
    sr = readstring(fr)
    close(fr)
    lrs_mot = [parse(Int,x) for x in split(sr,"\t")]
    return bwt_mot,lcp_mot,lrs_mot
end


function BWT_light(mot)
    # @time suffixsort(merge_mot)

    sa = suffixsort(merge_mot)
    bwt_mot = []
    lcp_mot = []
    lrs_mot = []
    lrs_value = 0
    for (i,x) in enumerate(sa.index)
        if x == 0
            push!(bwt_mot,merge_mot[length(merge_mot)])
        else
            push!(bwt_mot,merge_mot[x])
        end
        lrs_value = first_el(mot_turn(mot,x),'$')
        push!(lrs_mot,lrs_value)
        if i == 1
            push!(lcp_mot,0)
        else
            push!(lcp_mot,min(lrs_value,prefix(mot_turn(mot,sa.index[i-1]),mot_turn(mot,x))))
        end

    end
    return bwt_mot,lcp_mot,lrs_mot
end




function BWT_light2(mot,bwt_mot,lcp_mot,lrs_mot)
    sa = suffixsort(mot)
    lrs_value = 0
    # bwt_mot = fill(' ',length(mot))
    for (i,x) in enumerate(sa.index)
        if x == 0
            bwt_mot[i] = merge_mot[length(merge_mot)]
        else
            bwt_mot[i] = merge_mot[x]
        end
        lrs_value = first_el(mot_turn(mot,x),'$')
        lrs_mot[i] = lrs_value
        if i == 1
            lcp_mot[i]=0
        else
            lcp_mot[i] = min(lrs_value,prefix(mot_turn(mot,sa.index[i-1]),mot_turn(mot,x)))
        end
    end
    return bwt_mot
end






function mot_turn(mot,pos)
    if pos != 0
        return mot[pos+1:length(mot)]*mot[1:pos]
    else
        return mot
    end
end






function first_el(mot,lettre)
    for i in 1:length(mot)
        if mot[i] == lettre
            return i-1
        end
    end
    return length(mot)
end

function prefix(mot1,mot2)
    k = min(length(mot1),length(mot2))
    for i in 1:k
        if mot1[i] != mot2[i]
            return i-1
        end
    end
    return k
end


function visu(l1,l2,l3)
    k = min(length(l1),length(l2),length(l3))
    m = ""
    m0 = ""
    m1 = ""
    m2 = ""
    m3 = ""
    for i in 1:k
        m0 = m0*"$(i)\t"
        m1 = m1*"$(l1[i])\t"
        m2 = m2*"$(l2[i])\t"
        m3 = m3*"$(l3[i])\t"
        if (i % 10 == 0)
            m *= m0*"\n"*m1*"\n"*m2*"\n"*m3*"\n\n"
            m0 = ""
            m1 = ""
            m2 = ""
            m3 = ""
        end

    end
    m *=  m0*"\n"*m1*"\n"*m2*"\n"*m3
    return m
end

function color(m1)
    return "\033[01;33m$(m1)\033[0m"
end

function colorb(m1)
    return "\033[01;35m$(m1)\033[0m"
end

function visu_bwt(bwt,bo)
    m = ""
    for i in 1:length(bo)
        if bo[i]
            m *= color(bwt[i])
        else
            m *= "$(bwt[i])"
        end
    end
    return m
end

function visu_inter(bwt,node)
    m = ""
    aa = true
    for i in 1:length(node)
        if node[i]
            aa = !aa
        end
        if aa
            m *= color(bwt[i])
        else
            m *= colorb(bwt[i])
        end
    end
    return m
end

function visu_bool(l1)
    m = ""
    for i in 1:length(l1)
        if l1[i]
            m *= "."
        else
            m *= "-"
        end
    end
    return m
end

function nb_true(l1)
    nb = 0
    for i in 1:length(l1)
        if l1[i]
            nb += 1
        end
    end
    return nb
end



















function Create_Node(lcp,lrs)
    node = fill(true,length(lcp))
    for i in 2:length(lcp)
        node[i] = lcp[i] != lrs[i]
    end
    return node
end

function fin_inter(l1,position)

    if position > length(l1)
        return position
    end

    if position == length(l1)
        return length(l1)
    end

    i = position+1
    while i <= length(l1) && !l1[i]
        i += 1
        if i >= length(l1)
            return length(l1)
        end
    end
    return i-1
end

function zone_modifiable(node)
    zone = fill(false,length(lcp))

    i = 1
    last = fin_inter(node,i)
    while i <= length(node)
        if i < last
            for j in i:last
                zone[j] = true
            end
        end
        i = last+1
        last = fin_inter(node,i)
    end
    return zone
end

function decomposition(node)
    d = []
    i = 1
    last = fin_inter(node,i)
    while i <= length(node)
        push!(d,i)
        i = fin_inter(node,i)+1
    end
    return d
end

function character(bwt,decomp)
    d = []
    for i in 1:length(decomp)
        if i == length(decomp)
            lastt = length(bwt)
        else
            lastt = decomp[i+1]-1
        end
        push!(d,copy([]))
        for j in decomp[i]:lastt
            if !(bwt[j] in d[i])
                push!(d[i],bwt[j])
            end
        end
    end
    return d
end

function dicharacter(bwt,decomp)
    d = []
    for i in 1:length(decomp)
        if i == length(decomp)
            lastt = length(bwt)
        else
            lastt = decomp[i+1]-1
        end
        push!(d,copy(Dict()))
        for j in decomp[i]:lastt
            if !(bwt[j] in collect(keys(d[i])))
                d[i][bwt[j]] = 1
            else
                d[i][bwt[j]] += 1
            end
        end
    end
    return d
end

function Intersection(bwt,decomp,chara)
    d::Array{Array{Char}} = []
    for i in 1:(length(decomp)-1)
        push!(d,copy([]))
        c1 = chara[i]
        c2 = chara[i+1]
        for j in 1:length(c1)
            if c1[j] in c2 && !(c1[j] in d[i])
                push!(d[i],c1[j])
            end
        end
    end
    return d
end


function New_bwt_one(bwt,decomp,inter,dichara)
    for position in 1:length(decomp)
        if is_singleton(dichara,position)
            println(decomp[position])
        end
    end
end

function is_singleton(dichara,position)
    return length(collect(keys(dichara[position]))) == 1
end

function is_interval_one(inter,position)
    return length(inter[position]) == 1
end




function New_Decomp_aux_one(decomp,inter,dichara,position)
    lettre = inter[position][1]
    # println(position)
    # println(lettre," ",dichara[position]," ",inter[position]," ",dichara[position+1])
    inter[position] = copy([])
    if !(is_singleton(dichara,position+1))
        # println("a",decomp[position])
        li_char2 = Dict()
        li_char2[lettre] = dichara[position+1][lettre]
        nb2 = li_char2[lettre]
        insert!(decomp,position+2,decomp[position+1]+nb2)
        pop!(dichara[position+1],lettre)
        insert!(dichara,position+1,li_char2)
        if position+1 <= length(inter)
            # println(inter[position+1]," ",lettre)
            deleteat!(inter[position+1], findin(inter[position+1], [lettre]))
            # pop!(inter[position+1],lettre)
        end
        insert!(inter,position+1,copy([]))
    end
    if !(is_singleton(dichara,position))
        # println("o",decomp[position])
        li_char1 = Dict()
        li_char1[lettre] = dichara[position][lettre]
        nb1 = li_char1[lettre]
        insert!(decomp,position+1,decomp[position+1]-nb1)
        # println("insert decomp ",decomp[position+1]-nb1)
        pop!(dichara[position],lettre)
        # println("pop ",lettre, " ", dichara[position])
        insert!(dichara,position+1,li_char1)
        # println("insert dichara ",li_char1)
        if position >= 2
            # println(inter[position-1]," ",lettre)
            # pop!(inter[position-1],lettre)
            deleteat!(inter[position-1], findin(inter[position-1], [lettre]))
        end
        insert!(inter,position,copy([]))
    end
    if position > 2
        return position - 2
    else
        return position
    end

end


function New_Decomp(decomp,inter,dichara)
    i = 1
    # println(length(decomp))
    # println(decomp)
    while i <= length(inter)
        if is_interval_one(inter,i)
            i = New_Decomp_aux_one(decomp,inter,dichara,i)

        else
            i+= 1
        end
    end
    # println(length(decomp))
    # println(decomp)
end

function New_BWT(bwt,decomp,inter,dichara)
    # println(decomp)
    for i in 1:length(decomp)
        if true
            # if i == length(decomp)
            #     last = length(bwt)
            # else
            #     last = decomp[i+1]-1
            # end
            pos = decomp[i]
            for k in 1:length(collect(keys(dichara[i])))
                lettre = collect(keys(dichara[i]))[k]
                for j in 1:dichara[i][lettre]
                    bwt[pos] = lettre
                    # if decomp[i] >= 620 && decomp[i] <= 640
                    #     println("$(decomp[i]) $(pos) $(bwt[pos])")
                    # end
                    pos += 1
                end
            end
            # if decomp[i] >= 620 && decomp[i] <= 640
            #     println("$(decomp[i]) $(i) $(dichara[i])")
            # end
        end
    end
end
# function New_BWT(bwt,decomp,inter,dichara)
#     for i in 1:length(decomp)
#         if is_singleton(dichara,i)
#             if i == length(decomp)
#                 last = length(bwt)
#             else
#                 last = decomp[i+1]-1
#             end
#             lettre = collect(keys(dichara[i]))[1]
#             for j in decomp[i]:last
#                 bwt[j] = lettre
#             end
#         end
#     end
# end


function intervalle(bwt_mot,decomp_mot,inter_mot,dichara_mot)
    modif_mot = fill(true,length(bwt_mot))
    for (position,x) in enumerate(decomp_mot)
        if is_singleton(dichara_mot,position)
            if x == length(bwt_mot)
                last = length(bwt_mot)
            else
                last = decomp_mot[position+1]-1
            end
            for i in decomp_mot[position]:last
                modif_mot[i] = false
            end
        end
    end
    return modif_mot
end

function other_BWT(bwt,decomp,inter,dichara)
    modif = intervalle(bwt,decomp,inter,dichara)
    # global glob_modif = modif
    # println(modif[decomp[143]:decomp[144]])
    # println(modif[285:293])
    pos = 0
    l = []
    for j in 1:length(decomp)
        i = decomp[j]
        if !modif[i] || (j == length(decomp) || length(inter[j]) == 0)
            if pos != 0
                push!(l,[pos,j-1])
                pos = 0
            end
        else
            if pos == 0
                pos = j
            end
        end
    end
    if modif[length(modif)]
        if pos == 0
            push!(l,[length(decomp),length(decomp)])
        else
            push!(l,[pos,length(decomp)])
        end
    end
    # println([[x[1],x[2],decomp[x[1]],decomp[x[2]]] for x in l],"\n")
    for i in 1:length(l)
        modif_BWT(bwt,decomp,inter,dichara,l[i][1],l[i][2])
    end
end

function modif_BWT(bwt,decomp,inter,dichara,debut,fin)
    # println(debut,"\t",fin)
    if debut == fin
        return 0
    else
        last = '#'
        pos = decomp[debut]
        for i in debut:fin-1
            # println(i," ",inter[i]," ",dichara[i],dichara[i+1])
            if inter[i][1] == last
                lettre = inter[i][2]
            else
                lettre = inter[i][1]
            end
            # lettre = inter[i][1]
            # println("lettre\t$(lettre)\t$(last)")
            for (ii,x) in enumerate(collect(keys(dichara[i])))
                if x != lettre && x != last
                    for jj in 1:dichara[i][x]
                        bwt[pos] = x
                        # println("achar $(pos) $(bwt[pos])")
                        pos += 1
                    end
                end
            end
            for jj in 1:dichara[i][lettre]
                bwt[pos] = lettre
                # println("char $(pos) $(bwt[pos])")
                pos += 1
            end
            for jj in 1:dichara[i+1][lettre]
                bwt[pos] = lettre
                # println("vchar $(pos) $(bwt[pos])")
                pos += 1
            end
            last = lettre
        end
        for (ii,x) in enumerate(collect(keys(dichara[fin])))
            if x != last
                for jj in 1:dichara[fin][x]
                    bwt[pos] = x
                    # println("xchar $(pos) $(bwt[pos]) $(dichara[fin][x])")
                    pos += 1
                end
            end
        end
        # println(bwt[decomp[debut]:decomp[fin]])
    end
    return 0
end


function RLE(l1)
    nb = 0
    for i in 2:length(l1)
        if (l1[i-1] != l1[i])
            nb += 1
        end
    end
    return nb
end


function BWT_XBW(bwt_mot,lcp_mot,lrs_mot)
    decomp_table = Decomp_bwt(lcp_mot,lrs_mot)
    m = ""
    for i in 1:length(decomp_table)
        m *= mot_simple(bwt_mot,decomp_table[i])
    end
    return m
end

function mot_simple(bwt_mot,inter)
    m = ""
    l = []
    for i in inter[1]:inter[2]
        if !(bwt_mot[i] in l)
            push!(l,bwt_mot[i])
            m *= "$(bwt_mot[i])"
        end
    end
    return m
end

function Decomp_bwt(lcp_mot,lrs_mot)
    decomp_table = []
    k = min(length(lcp_mot),length(lrs_mot))
    j = 1
    for i in 1:k
        if (i != 1) && (lcp_mot[i] != lrs_mot[i])
            push!(decomp_table,[j,i-1])
            j = i
        end
    end
    push!(decomp_table,[j,k])
    return decomp_table
end



function Best_BWT2(bwt_mot,lcp_mot,lrs_mot)

    # println("q\t $(bwt_mot[632:636])")
    global glob_bwt = length(bwt_mot)
    node_mot = Create_Node(lcp_mot,lrs_mot)
    decomp_mot = decomposition(node_mot)
    global glob_node = length(decomp_mot)
    global glob_singleton = nombre_singleton(bwt_mot,decomp_mot)
    chara_mot = character(bwt_mot,decomp_mot)
    inter_mot = Intersection(bwt_mot,decomp_mot,chara_mot)
    dichara_mot = dicharacter(bwt_mot,decomp_mot)
    global glob_fixe = count([is_singleton(dichara_mot,position) for position in 1:length(decomp_mot)])

    global glob_move = zone_libre(bwt_mot,decomp_mot,dichara_mot)


    New_Decomp(decomp_mot,inter_mot,dichara_mot)

    # global glob_fixe2 = count([is_singleton(dichara_mot,position) for position in 1:length(decomp_mot)])

    New_BWT(bwt_mot,decomp_mot,inter_mot,dichara_mot)
    # println("n\t $(bwt_mot[632:636])")

    other_BWT(bwt_mot,decomp_mot,inter_mot,dichara_mot)
    # println("o\t $(bwt_mot[632:636])")

end

function BWT_XBW2(bwt_mot,lcp_mot,lrs_mot)
    node_mot = Create_Node(lcp_mot,lrs_mot)
    zone_mot = zone_modifiable(node_mot)
    decomp_mot = decomposition(node_mot)
    dechar_mot = decomposition_char(bwt_mot,decomp_mot)
    xbw_mot = fill(' ',length(dechar_mot))
    # println(length(dechar_mot))
    for i in 1:length(dechar_mot)
        xbw_mot[i] = bwt_mot[dechar_mot[i]]
    end
    if length(bwt_mot)<100
        println(visu_inter(bwt_mot,node_mot))
    end
    return xbw_mot
end

function decomposition_char(bwt_mot,decomp_mot)
    dechar_mot = []
    # println(length(decomp_mot))
    for i in 1:length(decomp_mot)
        # lettre = "!"
        if i == length(decomp_mot)
            last = length(bwt_mot)
        else
            last = decomp_mot[i+1]-1
        end
        l = []
        for j in decomp_mot[i]:last
            if !(bwt_mot[j] in l)
                push!(dechar_mot,j)
                push!(l,bwt_mot[j])
            end
        end
        if i == 370
            # println("$(i) $(l) $(decomp_mot[i]) $(last) $(bwt_mot[decomp_mot[i]:last])")
        end
    end
    return dechar_mot
end

function nombre_singleton(bwt_mot,decomp_mot)
    nb = 0
    for i in 1:length(decomp_mot)
        if i == length(decomp_mot)
            if decomp_mot[i] == length(bwt_mot)
                nb += 1
            end
        else
            if decomp_mot[i] == decomp_mot[i+1] - 1
                nb += 1
            end
        end
    end
    return nb
end

function zone_libre(bwt_mot,decomp_mot,dichara_mot)
    nb = 0
    for i in 1:length(decomp_mot)
        if i == length(decomp_mot)
            last = length(bwt_mot)
        else
            last = decomp_mot[i+1]-1
        end
        if !is_singleton(dichara_mot,i)
            nb += last- decomp_mot[i]  + 1
        end
    end
    return nb
end



function statistic(bwt_mot,lcp_mot,lrs_mot)
    println("Statistic:\t\t\tNb Positions\t\tBlock")
    println("\t #BWT: \t\t\t$(glob_bwt)\t\t\t$(glob_node)")
    println("\t #Singleton: \t\t$(glob_singleton)\t\t\t$(glob_singleton)")
    println("\t #Decomp 1 ≠ char: \t$(glob_bwt-glob_move)\t\t\t$(glob_fixe)")
    println("\t #Free: \t\t$(glob_move)\t\t\t$(glob_node-glob_fixe)")
end



function FLtable(sa,fl)
    # fl = zeros(length(sa))
    for (i,x) in enumerate(sa)
        if x == 0
            last = length(sa)-1
        else
            last = x - 1
        end
        # println(x," ",findin(sa,last)[1])
        fl[x+1] =  findin(sa,last)[1]
    end
    return fl
end











function di_mot(mot)
    di = Dict()
    for i in 1:length(mot)
        l = mot[i]
        if !(l in collect(keys(di)))
            di[l] = 0
        end
        di[l] += 1
    end
    return di
end

function eq_di(d1,d2)
    if collect(keys(d1)) != collect(keys(d2))
        return false
    end
    for (i,x) in enumerate(collect(keys(d1)))
        if d1[x] != d2[x]
            return false
        end
    end
    return true
end

bwt,lcp,lrs = BWT_file(ARGS[1],ARGS[2],ARGS[3])




bwt2 = copy(bwt)

println("Construction of BWT2 (with best RLE)")
@time Best_BWT2(bwt2,lcp,lrs)

node = Create_Node(lcp,lrs)
zone = zone_modifiable(node)
decomp = decomposition(node)

for i in 1:length(decomp)
    if i == length(decomp)
        last = length(bwt)
    else
        last = decomp[i+1]-1
    end
    m1 = join(bwt[decomp[i]:last])
    m2 = join(bwt2[decomp[i]:last])
    d1 = di_mot(m1)
    d2 = di_mot(m2)
    if !eq_di(d1,d2)
        println("$(decomp[i]) $(last) $(m1) $(m2)")
    end
end










println("Construction of XBW (of BWT)")

@time xbw = BWT_XBW2(bwt,lcp,lrs)

println("Construction of XBW2 (of BWT2)")

@time xbw2 = BWT_XBW2(bwt2,lcp,lrs)


statistic(bwt,lcp,lrs)

println("XBW:")

println("\t Size of BWT: \t$(length(bwt))")
println("\t Size of BWT2: \t$(length(bwt2))")
println("\t Size of XBW: \t$(length(xbw))")
println("\t Size of XBW2: \t$(length(xbw2))")


# println(join(xbw))
# println(join(xbw2))

println("RLE:")

println("\tRLE for BWT:\t",RLE(bwt))
println("\tRLE for XBW:\t",RLE(xbw))

println("\tRLE for BWT2:\t",RLE(bwt2), "\t" , round(RLE(bwt2)/RLE(bwt)*100*100)/100," % of RLE of BWT")
println("\tRLE for XBW2:\t",RLE(xbw2), "\t" , round(RLE(xbw2)/RLE(xbw)*100*100)/100," % of RLE of XBW")



lii = split(ARGS[1],".")
mi = join(lii[1:length(lii)-1],".")


println("Creation file $(mi*".bwt2")")
fo = open(mi*".bwt2","w")
write(fo,join(bwt2,"\t"))
close(fo)

println("Creation file $(mi*".xbw")")
fo = open(mi*".xbw","w")
write(fo,join(xbw,"\t"))
close(fo)

println("Creation file $(mi*".xbw2")")
fo = open(mi*".xbw2","w")
write(fo,join(xbw2,"\t"))
close(fo)
