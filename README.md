Compress BWT
=========

What is it?
-----------

The compress BWT is a julia implementatin of optimize the RLE for the concatenation of a set of strings.


Requirements
------------

* Julia version 0.6 or higher
    https://julialang.org/downloads/
* SDSL-lite
    https://github.com/simongog/sdsl-lite


How it works
------------

* Build build_bwt.cpp

```sh
make

```

* Build a file of a generate set of strings

```sh
julia creation_set_strings.jl 2000 15

```

* Build the BWT, LCP and LRS Tables

```sh
./build_bwt Input/a-2000-15.data

```

* Build the BWT2, XBW and XBW2 Tables

```sh
julia compression_bwt_file.jl Input/a-100-10.data.bwt Input/a-100-10.data.lcp Input/a-100-10.data.lrs

```

