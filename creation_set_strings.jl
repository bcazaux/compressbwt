function mot(long_mot::Int,alphabet::Array{Char})
    m::String = ""
    for i in 1:long_mot
        m = m*"$(rand(alphabet))"
    end
    return m
end

function ensemble_mots(nb_mot::Int,long_mot::Int,alphabet::Array{Char})
    P::Array{String} = []
    while length(P) < nb_mot
        m = mot(long_mot,alphabet)
        push!(P,m)
    end
    return P
end




nb = parse(Int,ARGS[1])
lg = parse(Int,ARGS[2])
if length(ARGS) < 3
	alph = ['a','b','c','d']
else
	alph = [x for x in ARGS[3]]
end



set_mot = ensemble_mots(nb,lg,alph)

println("Generation of  a set of strings S of $(nb) strings of length $(lg) over the alphabet $(alph)!")

file = "Input/a-$(ARGS[1])-$(ARGS[2]).data"

println("Creation file $(file)")

fall = open(file,"w")

for (i,x) in enumerate(set_mot)
    write(fall,x*"\n")
end

close(fall)
